package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskDisplayByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-display-by-project-id";

    @NotNull
    private final String DESC = "Display task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY TASKS BY PROJECT ID]");
        System.out.println("[ENTER ID]:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(getUserId(), projectId);
        renderTasks(tasks);
    }

}
