package ru.t1.ktubaltseva.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public class SortIncorrectException extends AbstractFieldException {

    public SortIncorrectException() {
        super("Error! Sort is incorrect...");
    }

    public SortIncorrectException(@NotNull final String value) {
        super("Error! This sort value \"" + value + "\" is incorrect...");
    }

}